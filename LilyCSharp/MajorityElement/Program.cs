﻿int MajorityElement(int[] nums)
{
    int size = nums.Length;
    List<int> dejaComparelist = new List<int>();

    for (int i = 0; i < size; i++)
    {
        int count = 0;

        if (!dejaComparelist.Contains(nums[i]))
        { 
            for(int j = 0; j < size; j++)
            {
                if (nums[j] == nums[i])
                {
                    count++;
                    if (count > size / 2)
                    {
                        return nums[i];
                    }

                }
            }

            dejaComparelist.Add(nums[i]);
        }
    }

    return 0;
}
int[] numArray = {3, 3, 3, 3, 1, 1, 1, 1, 1};

Console.WriteLine(MajorityElement(numArray));
Console.ReadKey();
